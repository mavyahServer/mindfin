import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { SuperadminService } from '../../superadmin.service';
import { CommonService } from '../../common.service';
import { Router } from '@angular/router';


export interface User {
  name: string;
}
@Component({
  templateUrl: './employee.component.html',
})

export class EmployeeComponent {
  myControl = new FormControl();
  val: any = [];
  selectedFile: File = null;
  selectedFile1: File = null;
  selectedFile2: File = null;

  constructor(private service: SuperadminService, private commonservice: CommonService,private router: Router ) { }


  model: any = {};
  fetchData: any;
  fetchData1: any;
  fetchData2: any;
  dob: any;
  createdby:any;
  value1:any;


  ngOnInit() {
    this.commonservice.getuserlist().subscribe(res => {
      console.log(res);
      this.fetchData = res;
    });



  }
  orgValueChange(date) {
    //console.log('');
    this.dob = date;
  }


  displayFn(user?: User): string | undefined {
    return user ? user.name : undefined;

  }

  onFileSelected(event) {
    console.log(event);
    this.selectedFile = <File>event.target.files[0];
  }
  onFileSelected1(event) {
    console.log(event);
    this.selectedFile1 = <File>event.target.files[0];
  }
  onFileSelected2(event) {
    console.log(event);
    this.selectedFile2 = <File>event.target.files[0];
  }
  submitForm(value) {
    // console.log(value);
    // const fd = new FormData();
    // if (this.selectedFile != null) {
    //   fd.append('cimage', this.selectedFile, this.selectedFile.name);
    // }
    // else {
    //   fd.append('cimage', "admin.png");
    // }


    // if (this.selectedFile1 != null) {
    //   fd.append('pimage', this.selectedFile, this.selectedFile.name);
    // }
    // else {
    //   fd.append('pimage', "admin.png");
    // }

    // if (this.selectedFile2 != null) {
    //   fd.append('aimage', this.selectedFile, this.selectedFile.name);
    // }
    // else {
    //   fd.append('aimage', "admin.png");
    // }


    
//     fd.append('name', this.model.name);
//     fd.append('dob', this.model.dob);

//     fd.append('mobile', this.model.mobile);
//     fd.append('email', this.model.email);
//     fd.append('altmobile', this.model.altmobile);
//     fd.append('pincode', this.model.pincode);

//     fd.append('address', this.model.address);


//     fd.append('gender', this.model.gender);
//     fd.append('accno', this.model.accno);
//     fd.append('ifsc', this.model.ifsc);
//     fd.append('qualification', this.model.qualification);
//     fd.append('branch', this.model.branch);
//     fd.append('iduser', this.model.iduser);
//     fd.append('joiningdate', this.model.joiningdate);
// fd.append('designation',this.model.designation);
// fd.append('empno',this.model.empno);


    console.log(value);
    this.createdby=localStorage.getItem("id")
    this.value1={value:value,createdby:this.createdby};
    console.log(this.value1);
    this.commonservice.employeeadd(this.value1)

      .subscribe(res => {
        this.router.navigate(["/member/employeelist"]);
      })

  }
  refresh(): void {
    window.location.reload();
  }

  checkcurrent(email) {
    console.log(email);
    this.commonservice.checkcurrent(email).subscribe(res => {
      console.log(res['status']);
      this.model.status = res['status'];
    })
  }


}