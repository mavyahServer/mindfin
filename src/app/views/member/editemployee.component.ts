import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { SuperadminService } from '../../superadmin.service';
import { CommonService } from '../../common.service';


export interface User {
name: string;
  }
@Component({
  templateUrl:'./editemployee.component.html',
})

export class EditemployeeComponent {
myControl = new FormControl();
val:any=[];
selectedFile:File = null;
selectedFile1:File = null;
selectedFile2:File = null;

constructor(private route: ActivatedRoute,private router:Router,private service:SuperadminService,private commonservice: CommonService){}

// options: User[] = this.val;
// filteredOptions: Observable<User[]>;
model:any={};
fetchData:any;
fetchData1:any;
fetchData2:any;
dob:any;
ciimage:any;
idvalue;
checkcurrent(email) {
  console.log(email);
  this.commonservice.checkcurrent(email).subscribe(res => {
    console.log(res['status']);
    this.model.status = res['status'];
  })
}
ngOnInit() {
//   this.commonservice.getloanlist().subscribe(res=>{
//     console.log(res);
// //  this.ciimage  = res[].cimage;
//     this.fetchData = res;
//   });
  // this.commonservice.getexecutivelist().subscribe(res=>{
  //   console.log(res);
  //   this.fetchData1 = res;
  // });
  // this.commonservice.getbanklist().subscribe(res=>{
  //   console.log(res);
  //   this.fetchData2 = res;
  // });
  this.route.params.subscribe(params=>{
    this.idvalue = params['id'];

  this.commonservice.editemp(params['id']).subscribe(res => {
    console.log(res);
    this.model=res[0];
this.model.iduser=this.model['idemployee'];
console.log(this.model.iduser);
  });
})
// this.commonservice.getexecutivelist().subscribe(res=>{
//   this.fetchData1 = [];
//   console.log(res);
//   for(var i=0;i<Object.keys(res).length;i++){
//     if(res[i].iduser!=null){
//     this.fetchData1.push(res[i]);
//     }
//   }
  
// });
this.commonservice.getuserlist().subscribe(res=>{
    console.log(res);
    this.fetchData = res;
  });
}
orgValueChange(date){
  //console.log('');
  this.dob=date;
}


displayFn(user?: User): string | undefined {
  return user ? user.name : undefined;
  
}

    onFileSelected(event){
    console.log(event);
    this.selectedFile = <File>event.target.files[0];
    // const fd=new FormData();

  }

  onFileSelected1(event){
    console.log(event);
    this.selectedFile1 = <File>event.target.files[0];
  }
  onFileSelected2(event){
    console.log(event);
    this.selectedFile2 = <File>event.target.files[0];
  }


submitForm(value){
console.log(value);
    const fd=new FormData();

    if(this.selectedFile!=null){
      fd.append('cimage',this.selectedFile,this.selectedFile.name);
     }
     else
     {
      fd.append('cimage',this.model.cimage);
     }
     if(this.selectedFile1!=null){
      fd.append('pimage',this.selectedFile1,this.selectedFile1.name);
     }
     else
     {
      fd.append('pimage',this.model.pimage);
     }
     if(this.selectedFile2!=null){
      fd.append('aimage',this.selectedFile2,this.selectedFile2.name);
     }
     else
     {
      fd.append('aimage',this.model.aimage);
     }
    // fd.append('cimage',this.selectedFile,this.selectedFile.name);
    // fd.append('pimage',this.selectedFile1,this.selectedFile1.name);
    // fd.append('aimage',this.selectedFile2,this.selectedFile2.name);
    fd.append('name',this.model.name);
    fd.append('dob',this.model.dob);
    fd.append('mobile',this.model.mobile);
    fd.append('email',this.model.email);
    fd.append('altmobile',this.model.altmobile);
    fd.append('pincode',this.model.pincode);
    fd.append('address',this.model.address);
    fd.append('gender',this.model.gender);
    fd.append('accno',this.model.accno);
    fd.append('ifsc',this.model.ifsc);
    fd.append('qualification',this.model.qualification);
    fd.append('branch',this.model.branch);
    fd.append('idusertype',this.model.idusertype);
    fd.append('id',this.idvalue);
    fd.append('joiningdate',this.model.joiningdate);
    fd.append('designation',this.model.designation);
    fd.append('empno',this.model.empno);


    // fd.append('name',this.model.dob);



    console.log(fd);
     this.commonservice.editemployee(fd);
    this.router.navigate(["/member/employeelist"]);
    // this.router.navigate(["/members/approval"]);

    //  this.refresh();
}
refresh(): void {
    window.location.reload();
  }
}