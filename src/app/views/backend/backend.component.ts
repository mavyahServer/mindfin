import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../../common.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-backend',
  templateUrl: './backend.component.html',
  // styleUrls: ['./addadmin.component.scss']
})
export class BackendComponent implements OnInit {

  myControl = new FormControl();
  val: any = [];
  selectedFile: File = null;
  selectedFile1: File = null;
  selectedFile2: File = null;
  selectedFile3: File = null;
  selectedFile4: File = null;
  selectedFile5: File = null;
  empid:any;
  empname:any;

  constructor(private route: ActivatedRoute, private router: Router, private commonservice: CommonService) { }

  obj: any;
  fetchData: any;
  model: any = {};


  ngOnInit() {
    this.commonservice.getexecutivelist().subscribe(res => {
      this.fetchData = [];
      console.log(res);
      for (var i = 0; i < Object.keys(res).length; i++) {
        if (res[i].iduser != null) {
          this.fetchData.push(res[i]);
        }
      }
    });
  }
  onFileSelected(event) {
    console.log(event);
    this.selectedFile = <File>event.target.files[0];
  }
  onFileSelected1(event) {
    console.log(event);
    this.selectedFile1 = <File>event.target.files[0];
  }
  onFileSelected2(event) {
    console.log(event);
    this.selectedFile2 = <File>event.target.files[0];
  }
  onFileSelected3(event) {
    console.log(event);
    this.selectedFile3 = <File>event.target.files[0];
  }
  onFileSelected4(event) {
    console.log(event);
    this.selectedFile4 = <File>event.target.files[0];
  }
  onFileSelected5(event) {
    console.log(event);
    this.selectedFile5 = <File>event.target.files[0];
  }
  submitForm(value) {
    console.log(value);
    this.empid=localStorage.getItem("id");
    this.empname=localStorage.getItem("empname");
    var abc = this.model.executiveid.split(",", 2);
    const fd = new FormData();
    if (this.selectedFile != null) {
      fd.append('companykyc', this.selectedFile, this.selectedFile.name);
    }else {
      fd.append('companykyc', "admin.png");
    }if (this.selectedFile1 != null) {
      fd.append('customerkyc', this.selectedFile1, this.selectedFile1.name);
    }else {
      fd.append('customerkyc', "admin.png");
    }if (this.selectedFile2 != null) {
      fd.append('itr', this.selectedFile2, this.selectedFile2.name);
    }else {
      fd.append('itr', "admin.png");
    }if (this.selectedFile3 != null) {
      fd.append('bankstatement', this.selectedFile3, this.selectedFile3.name);
    }else {
      fd.append('bankstatement', "admin.png");
    }if (this.selectedFile4 != null) {
      fd.append('loanstatement', this.selectedFile4, this.selectedFile4.name);
    }else {
      fd.append('loanstatement', "admin.png");
    }if (this.selectedFile5 != null) {
      fd.append('gstandreturns', this.selectedFile5, this.selectedFile5.name);
    }else {
      fd.append('gstandreturns', "admin.png");
    }
    fd.append('companyname', this.model.companyname);
    fd.append('customername', this.model.customername);
    fd.append('whosecase', this.model.whosecase);
    fd.append('executiveid', abc[0]);
    fd.append('executivename',abc[1])
    fd.append('createdbyname',this.empname)
    fd.append('displaystatus', this.model.displaystatus);
    fd.append('comment',this.model.comment);
    if(this.model.displaystatus != 'APPROVED'){
      fd.append('status',"PENDING")
          }
          else{
            fd.append('status',"APPROVED")
          }
    fd.append('createdby',this.empid);
    fd.append('mobile',this.model.mobile);
    fd.append('aadharno',this.model.aadharno);
    fd.append('panno',this.model.panno);
    console.log(fd);
    this.commonservice.custdocument(fd)
      .subscribe(res => {
        // window.location.reload();
        this.router.navigate(["/backend/viewdocument"]);
      })
  }
  refresh(): void {
    window.location.reload();
  }

}
