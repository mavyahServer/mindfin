export const navItems8 = [
    
    {
      name: 'My Profile',
      url: '/member/home',
      icon: 'fa fa-address-book-o',
    },
    {
      name: 'Add Employee',
      url: '/member/employee',
      icon: 'fa fa-briefcase'
    },
    {
      name: 'Tele Data',
      url: '/executives/admindata',
      icon: 'icon-user-following'
    },
    {
      name: 'Assigned Executive',
      url: '/executives/assignedlist',
      icon: 'fa fa-list-ul'
    },
    
      {
        name:'CHANGE PASSWORD',
        url:'/member/changepwd',
        icon:'icon-settings'
    }

  ];
  
